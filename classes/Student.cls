Class User.Student Extends (%Persistent, %Populate, %XML.Adaptor, %ZEN.DataModel.Adaptor, User.Person) [ SqlTableName = Student ]
{

Property index As %String [ Required ];

Property group As %String [ Required ];

Relationship studiesAt As Faculty [ Cardinality = one, Inverse = students ];

Method introduce() As %String
{
	  Set intro = "STUDENT "
	  Set personal = "["_..pesel_" "_..name_" "_..surname_"]"
      Set addressInfo = "["_..address.city_"] "_ ..address.street_" "_..address.number_"/"_..address.postal
      Set studentInfo = "[INDEX:"_..group_" GROUP:"_..index_"]"
      Return intro_studentInfo_personal_addressInfo
}

}