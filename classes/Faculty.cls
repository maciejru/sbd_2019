Class User.Faculty Extends (%Persistent, %Populate, %XML.Adaptor, %ZEN.DataModel.Adaptor, User.Unit) [ SqlTableName = Faculty ]
{

Relationship belongs As Department [ Cardinality = one, Inverse = contains ];

Relationship students As Student [ Cardinality = many, Inverse = studiesAt ];

Method present() As %String
{
	set presentation = "Presenting "_..name_" faculty of "_..belongs.name_" department!"
	return presentation
}

Method getStudentsNumberByCity(city As %String) As %Integer
{
	Set counter = 0
	FOR index=1:1:..students.Count() {
		IF ..students.GetAt(index).address.city = city 
		{
			set counter = counter + 1
		}
	}
	Return counter
}

}