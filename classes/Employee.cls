Class User.Employee Extends (%Persistent, %Populate, %XML.Adaptor, %ZEN.DataModel.Adaptor, User.Person) [ SqlTableName = Employee ]
{

Property identificator As %String [ Required ];

Property salary As %Integer [ Required ];

Relationship worksAt As Department [ Cardinality = one, Inverse = employees ];

Method introduce() As %String
{
	Set introduction = "EMPLOYEE ["_..identificator_"]"_"["_..pesel_" "_..name_" "_..surname_"]"
    Return introduction
}

}