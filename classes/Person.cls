Class User.Person [ Abstract ]
{

Property type As %String(VALUELIST = ",teacher, student, manager, director, headhunter, cleaner");

Property pesel As %String [ Required ];

Property name As %String(MAXLEN = 20) [ Required ];

Property surname As %String(MAXLEN = 20) [ Required ];

Property address As Address;

Method introduce() As %String
{
}

}
