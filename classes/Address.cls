Class User.Address Extends (%Persistent, %Populate, %XML.Adaptor, %ZEN.DataModel.Adaptor) [ SqlTableName = Address ]
{

Property city As %String(VALUELIST = ",Warszawa, Gdańsk, Gdynia, Szczecin") [ Required ];

Property street As %String(VALUELIST = ",Krakowska, Urbańska, Deblikowska, Tymianowska") [ Required ];

Property number As %Integer [ Required ];

Property postal As %String [ Required ];

Method print() As %String
{
	Return "Address: "_..street_" "_..number_" in "_..city_", "_..postal
}
}