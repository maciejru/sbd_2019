Class User.Unit [ Abstract ]
{

Property name As %String [ Required ];

Property code As %String [ Required ];

Property address As Address [ Required ];

Method giveDescription() As %String
{
      Set codeName = "UNIT ["_..name_"] ["_ ..code_"]"
      Return codeName
}

}
