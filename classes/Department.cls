Class User.Department Extends (%Persistent, %Populate, %XML.Adaptor, %ZEN.DataModel.Adaptor, User.Unit) [ SqlTableName = Department ]
{

Relationship employees As Employee [ Cardinality = many, Inverse = worksAt ];

Relationship contains As Faculty [ Cardinality = many, Inverse = belongs ];

Method countEmployeesByType(type As %String) As %Integer
{
	Set counter = 0
	FOR index=1:1:..employees.Count() {
		IF ..employees.GetAt(index).type = type 
		{
			write "found a "_type_"!"
			set counter = counter + 1
		}
	}
	Return counter
}

Method getMeanSalary() As %Integer
{
	Set meanSalary = 0
	FOR index=1:1:..employees.Count() {
		set meanSalary = meanSalary + ..employees.GetAt(index).salary
	}
	set meanSalary = meanSalary / ..employees.Count()
	Return meanSalary
}

}